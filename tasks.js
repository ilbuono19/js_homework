// функция для вычисления суммы
function calculateSum(x, y){
  return x + y;
}

// Обновите код в фукнции calculateSum, чтобы следующие выражения работали ожидаемым образом
console.log(calculateSum(10, '10'));  // 20
console.log(calculateSum('5', '10')); // 15
console.log(calculateSum(8, 8.8));  // 16.8


// функция для получения рекомендаци по погоде
function getWeatherRecommendations(isSunny, isRainy, isSnowy) {
  console.log('It is rainy, take an umbrella.');
  console.log('It is sunny, use sun protection.');
  console.log('It is snow outside, do not forget the hat.');
  console.log('Snow and sun are outside, do not forget the hat and sunglasses.');
  console.log('Casual day, have a great one!');
}

// Используйте условные операторы в функции выше, чтобы следующие выражения работали ожидаемым образом
getWeatherRecommendations(true, false, false);  // It is sunny, use sun protection.
getWeatherRecommendations(false, true, false);  // It is rainy, take an umbrella.
getWeatherRecommendations(true, false, true);  // Snow and sun are outside, do not forget the hat and sunglasses.
getWeatherRecommendations(false, false, true);  // It is snow outside, do not forget the hat.
getWeatherRecommendations(false, false, false);  // Сasual day, have a great one!


// Функция для проверки значения на false, измените функцию таким образом, 
// чтобы только переменна boolean типа со значением false давала результат "Значение равно false"
function checkIfValIsFalse(value) {
    if (value == false) {
      console.log("Значение равно false");
  } else {
      console.log("Значение не равно false");
  }
}

checkIfValIsFalse(false); // "Значение равно false"
checkIfValIsFalse(0); // "Значение не равно false"
checkIfValIsFalse(''); // "Значение не равно false"
checkIfValIsFalse(null); // "Значение не равно false"


// Функция для демонстрации работы со строками
function stringOperations() {
    let greeting = "Hello, world!";

    // Вывод длины строки
    console.log(`Длина строки: ???`);

    // Получение символа строки по индексу
    console.log(`Символ по индексу 1: ???`);

    // Преобразование строки в верхний регистр
    console.log(`Строка в верхнем регистре: ???`);

    // Поиск подстроки в строке
    let searchTerm = "world";

    /// напишите код, который будет выводить "Строка содержит подстроку <searchTerm>" если searchTerm есть в greeting
    
    let firstName = "John";
    let lastName = "Doe";
    console.log(`Полное имя: ??? ???`);

    // получите подстроку world из строки hello world
    // let subString = ???
    //console.log(`Подстрока: ${subString}`);
}


